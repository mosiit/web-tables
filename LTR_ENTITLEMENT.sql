use impresario
GO
CREATE TABLE dbo.LTR_ENTITLEMENT_new (
	entitlement_no int NOT NULL IDENTITY(1,1) PRIMARY KEY CLUSTERED,
	entitlement_code char(10) NULL,
	entitlement_desc varchar(255) NULL,
	memb_level_no INT NOT NULL,
	ent_tkw_id int NOT NULL,
	ent_price_type_group int NOT NULL,
	num_ent int NULL,
	reset_type char(1) NOT NULL,
	decline_benefit char(1) NULL,
	allow_lapsed char(1) Null,
	date_to_compare char(1) NULL,
	custom_1 varchar(255) NULL,
	custom_2 varchar(255) NULL,
	custom_3 varchar(255) NULL,
	custom_4 varchar(255) NULL,
	custom_5 varchar(255) NULL,
	custom_6 varchar(255) NULL,
	custom_7 varchar(255) NULL,
	custom_8 varchar(255) NULL,
	custom_9 varchar(255) NULL,
	custom_10 varchar(255) NULL,
	inactive char(1) NULL,
	created_by char(8) NULL,
	create_dt datetime NULL,
	last_updated_by char(8) NULL,
	last_update_dt datetime NULL)
GO
SET IDENTITY_INSERT LTR_ENTITLEMENT_new ON 
GO


INSERT dbo.LTR_ENTITLEMENT_new (
	entitlement_no, entitlement_code, entitlement_desc, memb_level_no, ent_tkw_id, ent_price_type_group, num_ent, reset_type, 
	custom_1, custom_2, custom_3, custom_4, custom_5, custom_6, custom_7, custom_8, custom_9, custom_10, created_by, create_dt, last_updated_by, last_update_dt)
SELECT 
entitlement_no, entitlement_code, entitlement_desc, memb_level_no, ent_tkw_id, ent_price_type_group, num_ent, reset_type,
	custom_1, custom_2, custom_3, custom_4, custom_5, custom_6, custom_7, custom_8, custom_9, custom_10, created_by, create_dt, last_updated_by, last_update_dt
FROM dbo.LTR_ENTITLEMENT
GO	
SET IDENTITY_INSERT LTR_ENTITLEMENT_new OFF
GO
DROP TABLE LTR_ENTITLEMENT
GO
exec dbo.sp_rename @objname = 'LTR_ENTITLEMENT_NEW', @newname = 'LTR_ENTITLEMENT'
GO
GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES ON dbo.LTR_ENTITLEMENT TO ImpUsers
GO
ALTER TABLE dbo.LTR_ENTITLEMENT ADD FOREIGN KEY (memb_level_no) REFERENCES T_MEMB_LEVEL (memb_level_no)
ALTER TABLE dbo.LTR_ENTITLEMENT ADD CONSTRAINT UK_ENT UNIQUE (memb_level_no, ent_tkw_id, ent_price_type_group)
ALTER TABLE dbo.LTR_ENTITLEMENT ADD CONSTRAINT DF_LTR_ENT_created_by  DEFAULT (user_name()) FOR [created_by]
ALTER TABLE dbo.LTR_ENTITLEMENT ADD CONSTRAINT DF_LTR_ENT_create_dt  DEFAULT (getdate()) FOR [create_dt]
ALTER TABLE dbo.LTR_ENTITLEMENT ADD CONSTRAINT DF_LTR_ENT_inactive  DEFAULT ('N') FOR [inactive]
ALTER TABLE dbo.LTR_ENTITLEMENT ADD CONSTRAINT DF_LTR_ENT_decline_benefit DEFAULT ('Y') FOR [decline_benefit]
GO
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_ENT_TU]'))
	DROP TRIGGER dbo.LTR_ENT_TU
GO
CREATE TRIGGER dbo.LTR_ENT_TU ON dbo.LTR_ENTITLEMENT FOR UPDATE 
AS

SET NOCOUNT ON

UPDATE 	a
SET
	last_updated_by = user_name(),
	last_update_dt = getdate()
FROM dbo.LTR_ENTITLEMENT a
	JOIN inserted b ON a.entitlement_no = b.entitlement_no
GO
EXEC UP_POPULATE_REFERENCE_METADATA @table_name = 'LTR_ENTITLEMENT'
GO
IF NOT EXISTS (SELECT TOP 1 * FROM TR_GOOESOFT_DROPDOWN WHERE code = 1002)
  BEGIN
	Declare @gid int
	Select	@gid = MAX(gid) from TR_GOOESOFT_DROPDOWN

	Insert into TR_GOOESOFT_DROPDOWN (gid, code, id, description, short_desc ) VALUES ( @gid+1, 1002, 1, 'Daily', 'D' )
	Insert into TR_GOOESOFT_DROPDOWN (gid, code, id, description, short_desc ) VALUES ( @gid+2, 1002, 2, 'Membership', 'M' )
	Insert into TR_GOOESOFT_DROPDOWN (gid, code, id, description, short_desc ) VALUES ( @gid+3, 1002, 3, 'Performance', 'P' )
  END
GO
UPDATE dbo.LTR_ENTITLEMENT SET decline_benefit = default
GO

UPDATE dbo.TR_REFERENCE_COLUMN SET checkbox = 'Y' where column_name like 'decline_benefit' 
GO