Use impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_WEB_DONATION_CONFIG_ROUND]') AND type in (N'U')) 
    DROP TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_WEB_DONATION_CONFIG_ROUND]') AND type in (N'U')) BEGIN
    CREATE TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND](
        [id] [int] NOT NULL IDENTITY (1, 1),
	    [variant_id] [int] NOT NULL,
        [range_start] [decimal] (18,2) NOT NULL,
        [range_end] [decimal] (18,2) NULL,
        [value] [decimal] (18,2) NOT NULL,
        [min_value] [decimal] (18,2) NOT NULL,
        [offset] [int] NULL,
        [inactive] [char](1) NULL,
    	[created_by] [varchar](8) NULL,
	    [create_dt] [datetime] NULL,
    	[create_loc] [varchar](16) NULL,
	    [last_updated_by] [varchar](8) NULL,
    	[last_update_dt] [datetime] NULL,
      CONSTRAINT [PK_LTR_WEB_DONATION_CONFIG_ROUND] PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_ROUND_variant_id]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_ROUND_variant_id]  DEFAULT (0) FOR [variant_id]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_ROUND_range_start]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_ROUND_range_start]  DEFAULT (0.0) FOR [range_start]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_ROUND_range_end]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_ROUND_range_end]  DEFAULT (0.0) FOR [range_end]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_ROUND_value]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_ROUND_value]  DEFAULT (0.0) FOR [value]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_ROUND_min_value]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_ROUND_min_value]  DEFAULT (0.0) FOR [min_value]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_ROUND_offset]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_ROUND_offset]  DEFAULT (0) FOR [offset]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_ROUND_inactive]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_ROUND_inactive]  DEFAULT ('N') FOR [inactive]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_ROUND_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_ROUND_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_ROUND_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_ROUND_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_ROUND_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_ROUND_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_ROUND_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_ROUND_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_ROUND_last_update_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_ROUND_last_update_dt]  DEFAULT (getdate()) FOR [last_update_dt]
END
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] TO impusers
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_WEB_DONATION_CONFIG_ROUND'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_WEB_DONATION_CONFIG_ROUND_TU]'))
    DROP TRIGGER [dbo].[LTR_WEB_DONATION_CONFIG_ROUND_TU]
GO

CREATE TRIGGER [dbo].[LTR_WEB_DONATION_CONFIG_ROUND_TU] ON [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_WEB_DONATION_CONFIG_ROUND]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_WEB_DONATION_CONFIG_ROUND] a, inserted b
    WHERE	a.id = b.id
 
END
GO

INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	5.00,	6.49,	7.50,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	6.50,	9.99,	10.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	10.00,	14.99,	15.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	15.00,	19.99,	20.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	20.00,	24.99,	25.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	25.00,	29.99,	30.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	30.00,	34.99,	35.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	35.00,	39.99,	40.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	40.00,	44.99,	45.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	45.00,	49.99,	50.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	50.00,	54.99,	60.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	55.00,	59.99,	65.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	60.00,	64.99,	70.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	65.00,	69.99,	75.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	70.00,	74.99,	80.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	75.00,	79.99,	85.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	80.00,	84.99,	90.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	85.00,	89.99,	100.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	90.00,	94.99,	105.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	95.00,	99.99,	110.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	100.00,	104.99,	115.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	105.00,	109.99,	120.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	110.00,	114.99,	125.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	115.00,	119.99,	130.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	120.00,	124.99,	135.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	125.00,	129.99,	140.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	130.00,	134.99,	150.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	135.00,	139.99,	155.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	140.00,	144.99,	160.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	145.00,	149.99,	165.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	150.00,	154.99,	170.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	155.00,	159.99,	175.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	160.00,	164.99,	180.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	165.00,	169.99,	185.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	170.00,	174.99,	190.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	175.00,	179.99,	200.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	180.00,	184.99,	205.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	185.00,	189.99,	210.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	190.00,	194.99,	215.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	195.00,	199.99,	220.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	200.00,	204.99,	225.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	205.00,	209.99,	230.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	210.00,	214.99,	235.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	215.00,	219.99,	240.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	220.00,	224.99,	245.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	225.00,	229.99,	250.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	230.00,	234.99,	255.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	235.00,	239.99,	260.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	240.00,	244.99,	265.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	245.00,	249.99,	270.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	250.00,	254.99,	275.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	255.00,	259.99,	280.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	260.00,	264.99,	285.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	265.00,	269.99,	290.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	270.00,	274.99,	295.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	275.00,	279.99,	300.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	280.00,	284.99,	305.00,	1.5,    2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	285.00,	289.99,	310.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	290.00,	294.99,	315.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	295.00,	299.99,	320.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	300.00,	304.99,	325.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	305.00,	309.99,	330.00,	1.5,	2)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_ROUND] ([variant_id], [range_start], [range_end], [value], [min_value], [offset]) VALUES (1,	310.00, 0.00,	310.00,	10,	    10)
GO


SELECT * FROM [dbo].[LTR_WEB_DONATION_CONFIG_ROUND]

