USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO


CREATE TABLE LTR_CARD_UPDATES
(
	id INT IDENTITY NOT NULL,
	customer_no INT NOT NULL,
	payment_account_id INT NOT NULL,
	process_dt DATETIME NOT NULL,
 CONSTRAINT [PK_LTR_CARD_UPDATES] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_CARD_UPDATES] TO impusers
GO