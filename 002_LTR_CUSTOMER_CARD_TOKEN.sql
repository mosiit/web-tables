USE [Impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_CUSTOMER_CARD_TOKEN]') AND type in (N'U')) 
    DROP TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_CUSTOMER_CARD_TOKEN]') AND type in (N'U')) BEGIN
    CREATE TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN](
        [id] [int] NOT NULL IDENTITY (1, 1),
	    [customer_no] [int] NOT NULL,
        [payment_token] [varchar] (120) NULL,
        [last_digits] [varchar] (4) NULL,
        [expiry_month] [int] NULL,
        [expiry_year] [int] NULL,
        [card_type] [varchar] (120) NULL,
        [inactive] [char](1) NULL,
    	[created_by] [varchar](8) NULL,
	    [create_dt] [datetime] NULL,
    	[create_loc] [varchar](16) NULL,
	    [last_updated_by] [varchar](8) NULL,
    	[last_update_dt] [datetime] NULL,
      CONSTRAINT [PK_LTR_CUSTOMER_CARD_TOKEN] PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CUSTOMER_CARD_TOKEN_customer_no]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN] ADD  CONSTRAINT [DF_LTR_CUSTOMER_CARD_TOKEN_customer_no]  DEFAULT (0) FOR [customer_no]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CUSTOMER_CARD_TOKEN_payment_token]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN] ADD  CONSTRAINT [DF_LTR_CUSTOMER_CARD_TOKEN_payment_token]  DEFAULT ('') FOR [payment_token]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CUSTOMER_CARD_TOKEN_last_digits]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN] ADD  CONSTRAINT [DF_LTR_CUSTOMER_CARD_TOKEN_last_digits]  DEFAULT ('') FOR [last_digits]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CUSTOMER_CARD_TOKEN_expiry_month]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN] ADD  CONSTRAINT [DF_LTR_CUSTOMER_CARD_TOKEN_expiry_month]  DEFAULT (0) FOR [expiry_month]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CUSTOMER_CARD_TOKEN_expiry_year]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN] ADD  CONSTRAINT [DF_LTR_CUSTOMER_CARD_TOKEN_expiry_year]  DEFAULT (0) FOR [expiry_year]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CUSTOMER_CARD_TOKEN_card_type]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN] ADD  CONSTRAINT [DF_LTR_CUSTOMER_CARD_TOKEN_card_type]  DEFAULT ('') FOR [card_type]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CUSTOMER_CARD_TOKEN_inactive]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN] ADD  CONSTRAINT [DF_LTR_CUSTOMER_CARD_TOKEN_inactive]  DEFAULT ('N') FOR [inactive]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CUSTOMER_CARD_TOKEN_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN] ADD  CONSTRAINT [DF_LTR_CUSTOMER_CARD_TOKEN_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CUSTOMER_CARD_TOKEN_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN] ADD  CONSTRAINT [DF_LTR_CUSTOMER_CARD_TOKEN_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CUSTOMER_CARD_TOKEN_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN] ADD  CONSTRAINT [DF_LTR_CUSTOMER_CARD_TOKEN_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CUSTOMER_CARD_TOKEN_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN] ADD  CONSTRAINT [DF_LTR_CUSTOMER_CARD_TOKEN_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_CUSTOMER_CARD_TOKEN_last_update_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_CUSTOMER_CARD_TOKEN] ADD  CONSTRAINT [DF_LTR_CUSTOMER_CARD_TOKEN_last_update_dt]  DEFAULT (getdate()) FOR [last_update_dt]
END
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_CUSTOMER_CARD_TOKEN] TO impusers
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_CUSTOMER_CARD_TOKEN'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_CUSTOMER_CARD_TOKEN_TU]'))
    DROP TRIGGER [dbo].[LTR_CUSTOMER_CARD_TOKEN_TU]
GO

CREATE TRIGGER [dbo].[LTR_CUSTOMER_CARD_TOKEN_TU] ON [dbo].[LTR_CUSTOMER_CARD_TOKEN] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_CUSTOMER_CARD_TOKEN]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_CUSTOMER_CARD_TOKEN] a, inserted b
    WHERE	a.id = b.id
 
END
GO


SELECT * FROM [dbo].[LTR_CUSTOMER_CARD_TOKEN]