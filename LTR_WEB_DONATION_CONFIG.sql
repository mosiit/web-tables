USE [impresario]
GO
/****** Object:  Table [dbo].[LTR_WEB_DONATION_CONFIG] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
    ●   ●       ● ●           ●     ● ●
  ●   ●   ●         ●     ● ● ●   ●     ●
  ●   ●   ●     ● ● ●   ●     ●   ●   ●
  ●   ●   ●   ●     ●   ●     ●   ●
  ●   ●   ●     ● ●       ● ●       ● ●
© Made Media Ltd.
http://made.media
*/

-- Delete the old table if it exists
IF OBJECT_ID('dbo.LTR_WEB_DONATION_CONFIG', 'U') IS NOT NULL 
   DROP TABLE dbo.LTR_WEB_DONATION_CONFIG; 
GO

CREATE TABLE dbo.LTR_WEB_DONATION_CONFIG (
        id INT NOT NULL
               IDENTITY
               PRIMARY KEY,
        variant_id VARCHAR(255),
        message_title VARCHAR(255),
        description TEXT,
        fund_id INT,
        prod_season_no INT,
        donate_by_default BIT,
        inactive CHAR(1),
        range_min DECIMAL(18, 2),
        range_max DECIMAL(18, 2),
        method_id INT,
        value DECIMAL(18, 2),
        created_by VARCHAR(8),
        create_dt DATETIME,
        create_loc VARCHAR(16),
        last_updated_by VARCHAR(8),
        last_update_dt DATETIME
       )

GO
GRANT SELECT,INSERT,UPDATE,DELETE ON [dbo].[LTR_WEB_DONATION_CONFIG] TO impusers
GO

EXEC UP_POPULATE_REFERENCE_METADATA;

UPDATE  TR_REFERENCE_COLUMN
SET     order_across = 1,
        dddw_table = 'LTR_DONATION_CONFIG_METHOD',
        dddw_value = 'id',
        dddw_description = 'description'
FROM    TR_REFERENCE_TABLE t
JOIN    TR_REFERENCE_COLUMN c
ON      t.id = c.reference_table_id
WHERE   table_name = 'LTR_WEB_DONATION_CONFIG'
        AND column_name = 'method_id';

GO