USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO

CREATE TABLE dbo.LTR_TEACHER_ORDERS
(
    id INT NOT NULL IDENTITY PRIMARY KEY,
    teacher_customer_no INT NOT NULL,
    school_customer_no INT NOT NULL,
    school_order_no INT NOT NULL
);

SET ANSI_PADDING OFF
GO


GO
GRANT SELECT, INSERT, DELETE, UPDATE ON [dbo].[LTR_TEACHER_ORDERS] TO ImpUsers;
GO
