use impresario
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_ENTITLEMENT_PRICE_MAPPING]') AND type IN (N'U'))
	DROP TABLE [dbo].[LTR_ENTITLEMENT_PRICE_MAPPING]
GO
CREATE TABLE dbo.LTR_ENTITLEMENT_PRICE_MAPPING (
	entitlement_price_no int IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,
	source_price_type int null,
	target_price_type int null,
	created_by char(8) NULL,
	create_dt datetime NULL,
	last_updated_by char(8) NULL,
	last_update_dt datetime NULL)
GO
GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES ON dbo.LTR_ENTITLEMENT_PRICE_MAPPING TO ImpUsers
GO
ALTER TABLE dbo.LTR_ENTITLEMENT_PRICE_MAPPING ADD CONSTRAINT DF_LTR_ENT_PRICE_MAP_created_by  DEFAULT (user_name()) FOR [created_by]
ALTER TABLE dbo.LTR_ENTITLEMENT_PRICE_MAPPING ADD CONSTRAINT DF_LTR_ENT_PRICE_MAP_create_dt  DEFAULT (getdate()) FOR [create_dt]
GO
EXEC dbo.UP_POPULATE_REFERENCE_METADATA @table_name = 'LTR_ENTITLEMENT_PRICE_MAPPING'
GO
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_ENTITLEMENT_PRICE_MAPPING_TU]'))
	DROP TRIGGER dbo.LTR_ENTITLEMENT_PRICE_MAPPING_TU
GO
CREATE TRIGGER dbo.LTR_ENTITLEMENT_PRICE_MAPPING_TU ON dbo.LTR_ENTITLEMENT_PRICE_MAPPING FOR UPDATE 
AS

SET NOCOUNT ON

UPDATE 	a
SET
	last_updated_by = user_name(),
	last_update_dt = getdate()
FROM dbo.LTR_ENTITLEMENT_PRICE_MAPPING a
	JOIN inserted b ON a.entitlement_price_no = b.entitlement_price_no
GO
/* lookups for entitlement price mapping */
UPDATE	TR_REFERENCE_COLUMN
SET		dddw_table = 'TR_PRICE_TYPE',
		dddw_value = 'id',
		dddw_description = 'description'
WHERE reference_table_id = (SELECT id FROM TR_REFERENCE_TABLE where description = 'LTR_ENTITLEMENT_PRICE_MAPPING')
AND column_name IN ('source_price_type','target_price_type')
GO