USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_WEB_TPP_TOKEN]') AND type in (N'U')) 
        DROP TABLE [dbo].[LTR_WEB_TPP_TOKEN]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_WEB_TPP_TOKEN]') AND type in (N'U')) BEGIN
    CREATE TABLE [dbo].[LTR_WEB_TPP_TOKEN](
        [id] [int] NOT NULL IDENTITY (1, 1),
		[school_id] INT, 
        [email] [varchar] (100) NOT NULL,
        [token] [varchar] (250) NOT NULL,
        [subjects] [varchar] (500) NULL,
        [grades] [varchar] (500) NULL,
        [school] [varchar] (500) NULL,
        [address] [varchar] (500) NULL,
        [expiry] [date] NULL,
        [inactive] [char](1) NULL,
    	[created_by] [varchar](8) NULL,
	    [create_dt] [datetime] NULL,
    	[create_loc] [varchar](16) NULL,
	    [last_updated_by] [varchar](8) NULL,
    	[last_update_dt] [datetime] NULL,
      CONSTRAINT [PK_LTR_WEB_TPP_TOKEN] PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_TPP_TOKEN_email]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_TPP_TOKEN] ADD  CONSTRAINT [DF_LTR_WEB_TPP_TOKEN_email]  DEFAULT ('') FOR [email]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_TPP_TOKEN_token]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_TPP_TOKEN] ADD  CONSTRAINT [DF_LTR_WEB_TPP_TOKEN_token]  DEFAULT ('') FOR [token]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_TPP_TOKEN_subjects]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_TPP_TOKEN] ADD  CONSTRAINT [DF_LTR_WEB_TPP_TOKEN_subjects]  DEFAULT ('') FOR [subjects]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_TPP_TOKEN_grades]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_TPP_TOKEN] ADD  CONSTRAINT [DF_LTR_WEB_TPP_TOKEN_grades]  DEFAULT ('') FOR [grades]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_TPP_TOKEN_school]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_TPP_TOKEN] ADD  CONSTRAINT [DF_LTR_WEB_TPP_TOKEN_school]  DEFAULT ('') FOR [school]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_TPP_TOKEN_address]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_TPP_TOKEN] ADD  CONSTRAINT [DF_LTR_WEB_TPP_TOKEN_address]  DEFAULT ('') FOR [address]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_TPP_TOKEN_inactive]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_TPP_TOKEN] ADD  CONSTRAINT [DF_LTR_WEB_TPP_TOKEN_inactive]  DEFAULT ('N') FOR [inactive]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_TPP_TOKEN_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_TPP_TOKEN] ADD  CONSTRAINT [DF_LTR_WEB_TPP_TOKEN_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_TPP_TOKEN_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_TPP_TOKEN] ADD  CONSTRAINT [DF_LTR_WEB_TPP_TOKEN_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_TPP_TOKEN_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_TPP_TOKEN] ADD  CONSTRAINT [DF_LTR_WEB_TPP_TOKEN_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_TPP_TOKEN_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_TPP_TOKEN] ADD  CONSTRAINT [DF_LTR_WEB_TPP_TOKEN_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_TPP_TOKEN_last_update_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_TPP_TOKEN] ADD  CONSTRAINT [DF_LTR_WEB_TPP_TOKEN_last_update_dt]  DEFAULT (getdate()) FOR [last_update_dt]
END
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_WEB_TPP_TOKEN] TO impusers
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_WEB_TPP_TOKEN'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_WEB_TPP_TOKEN_TU]'))
    DROP TRIGGER [dbo].[LTR_WEB_TPP_TOKEN_TU]
GO

CREATE TRIGGER [dbo].[LTR_WEB_TPP_TOKEN_TU] ON [dbo].[LTR_WEB_TPP_TOKEN] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_WEB_TPP_TOKEN]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_WEB_TPP_TOKEN] a, inserted b
    WHERE	a.id = b.id
 
END
GO

SELECT * FROM [dbo].[LTR_WEB_TPP_TOKEN]


--DISABLE TRIGGER [dbo].[LTR_WEB_TPP_TOKEN_TU]  ON [dbo].[LTR_WEB_TPP_TOKEN]
--GO
--SET IDENTITY_INSERT [dbo].[LTR_WEB_TPP_TOKEN] ON
--GO
--INSERT INTO [dbo].[LTR_WEB_TPP_TOKEN] (id, email, token, subjects, grades, school, expiry, inactive, created_by, create_dt, create_loc, last_updated_by, last_update_dt)
--SELECT id, email, token, subjects, grades, school, expiry, inactive, created_by, create_dt, create_loc, last_updated_by, last_update_dt FROM [dbo].[LTR_WEB_TPP_TOKEN_TEMP]
--GO
--SET IDENTITY_INSERT [dbo].[LTR_WEB_TPP_TOKEN] OFF
--GO
--ENABLE TRIGGER [dbo].[LTR_WEB_TPP_TOKEN_TU]  ON [dbo].[LTR_WEB_TPP_TOKEN]
--GO




--SELECT * INTO [dbo].[LTR_WEB_TPP_TOKEN_TEMP] FROM [dbo].[LTR_WEB_TPP_TOKEN]
--SELECT id, email, token, subjects, grades, school, expiry, inactive, created_by, create_dt, create_loc, last_updated_by, last_update_dt FROM [dbo].[LTR_WEB_TPP_TOKEN_TEMP]