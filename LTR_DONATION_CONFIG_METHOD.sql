USE [impresario]
GO
/****** Object:  Table [dbo].[LTR_DONATION_CONFIG_METHOD] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
    ●   ●       ● ●           ●     ● ●
  ●   ●   ●         ●     ● ● ●   ●     ●
  ●   ●   ●     ● ● ●   ●     ●   ●   ●
  ●   ●   ●   ●     ●   ●     ●   ●
  ●   ●   ●     ● ●       ● ●       ● ●
© Made Media Ltd.
http://made.media
*/

-- Delete the old table if it exists
IF OBJECT_ID('dbo.LTR_DONATION_CONFIG_METHOD', 'U') IS NOT NULL 
   DROP TABLE dbo.LTR_DONATION_CONFIG_METHOD; 
GO

CREATE TABLE dbo.LTR_DONATION_CONFIG_METHOD (
        id INT NOT NULL
               IDENTITY
               PRIMARY KEY,
        description VARCHAR(255)
       )

GO
GRANT SELECT,INSERT,UPDATE,DELETE ON [dbo].[LTR_DONATION_CONFIG_METHOD] TO impusers
GO

-- Add tables to reference Table metadata

EXEC UP_POPULATE_REFERENCE_METADATA;
GO
