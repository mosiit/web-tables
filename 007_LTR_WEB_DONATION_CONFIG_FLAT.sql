USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_WEB_DONATION_CONFIG_FLAT]') AND type in (N'U')) 
    DROP TABLE [dbo].[LTR_WEB_DONATION_CONFIG_FLAT]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_WEB_DONATION_CONFIG_FLAT]') AND type in (N'U')) BEGIN
    CREATE TABLE [dbo].[LTR_WEB_DONATION_CONFIG_FLAT](
        [id] [int] NOT NULL IDENTITY (1, 1),
	    [variant_id] [int] NOT NULL,
        [rank] [int] NOT NULL,
        [range_start] [decimal] (18,2) NOT NULL,
        [range_end] [decimal] (18,2) NULL,
        [value] [varchar] (100) NULL,
        [inactive] [char](1) NULL,
    	[created_by] [varchar](8) NULL,
	    [create_dt] [datetime] NULL,
    	[create_loc] [varchar](16) NULL,
	    [last_updated_by] [varchar](8) NULL,
    	[last_update_dt] [datetime] NULL,
      CONSTRAINT [PK_LTR_WEB_DONATION_CONFIG_FLAT] PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_FLAT_variant_id]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_FLAT_variant_id]  DEFAULT (0) FOR [variant_id]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_FLAT_rank]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_FLAT_rank]  DEFAULT (0) FOR [rank]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_FLAT_range_start]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_FLAT_range_start]  DEFAULT (0.0) FOR [range_start]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_FLAT_range_end]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_FLAT_range_end]  DEFAULT (0.0) FOR [range_end]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_FLAT_value]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_FLAT_value]  DEFAULT (0.0) FOR [value]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_FLAT_inactive]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_FLAT_inactive]  DEFAULT ('N') FOR [inactive]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_FLAT_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_FLAT_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_FLAT_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_FLAT_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_FLAT_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_FLAT_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_FLAT_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_FLAT_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_FLAT_last_update_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_FLAT_last_update_dt]  DEFAULT (getdate()) FOR [last_update_dt]
END
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] TO impusers
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_WEB_DONATION_CONFIG_FLAT'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_WEB_DONATION_CONFIG_FLAT_TU]'))
    DROP TRIGGER [dbo].[LTR_WEB_DONATION_CONFIG_FLAT_TU]
GO

CREATE TRIGGER [dbo].[LTR_WEB_DONATION_CONFIG_FLAT_TU] ON [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_WEB_DONATION_CONFIG_FLAT]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_WEB_DONATION_CONFIG_FLAT] a, inserted b
    WHERE	a.id = b.id
 
END
GO

INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ([variant_id], [rank], [range_start], [range_end], [value]) VALUES (1, 1, 0.00, 49.99, 2.00)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ([variant_id], [rank], [range_start], [range_end], [value]) VALUES (1, 1, 50.00, 89.99, 5.00)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ([variant_id], [rank], [range_start], [range_end], [value]) VALUES (1, 1, 90.00, 189.99, 10.00)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ([variant_id], [rank], [range_start], [range_end], [value]) VALUES (1, 1, 190.00, 0.00, 15.00)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ([variant_id], [rank], [range_start], [range_end], [value]) VALUES (1, 2, 0.00, 49.99, 5.00)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ([variant_id], [rank], [range_start], [range_end], [value]) VALUES (1, 2, 50.00, 89.99, 10.00)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ([variant_id], [rank], [range_start], [range_end], [value]) VALUES (1, 2, 90.00, 189.99, 15.00)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] ([variant_id], [rank], [range_start], [range_end], [value]) VALUES (1, 2, 190.00, 0.00, 20.00)
GO

SELECT * FROM [dbo].[LTR_WEB_DONATION_CONFIG_FLAT] 