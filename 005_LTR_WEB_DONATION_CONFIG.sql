USE [Impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_WEB_DONATION_CONFIG]') AND type in (N'U'))
    DROP TABLE [dbo].[LTR_WEB_DONATION_CONFIG]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LTR_WEB_DONATION_CONFIG]') AND type in (N'U')) BEGIN
    CREATE TABLE [dbo].[LTR_WEB_DONATION_CONFIG](
        [id] [int] NOT NULL IDENTITY (1, 1),
	    
        [variant_id] [int] NOT NULL,
        [rank] [int] NOT NULL,
        [constituent_no] [int] NOT NULL,
        [message_title] [varchar] (255) NULL,
        [description] [text] NULL,
        [method_no] [varchar] (50) NULL,
        [fund_id] [int] NULL,
        [prod_season_no] [int] NULL,
        [donate_by_default] [bit] NOT NULL,
        [inactive] [char](1) NULL,
    	[created_by] [varchar](8) NULL,
	    [create_dt] [datetime] NULL,
    	[create_loc] [varchar](16) NULL,
	    [last_updated_by] [varchar](8) NULL,
    	[last_update_dt] [datetime] NULL,
      CONSTRAINT [PK_LTR_WEB_DONATION_CONFIG] PRIMARY KEY CLUSTERED ([id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_variant_id]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_variant_id]  DEFAULT (0) FOR [variant_id]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_rank]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_rank]  DEFAULT (0) FOR [rank]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_constituent_no]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_constituent_no]  DEFAULT (0) FOR [constituent_no]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_message_title]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_message_title]  DEFAULT ('') FOR [message_title]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_method_no]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_method_no]  DEFAULT ('') FOR [method_no]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_fund_id]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_fund_id]  DEFAULT (0) FOR [fund_id]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_prod_season_no]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_prod_season_no]  DEFAULT (0) FOR [prod_season_no]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_inactive]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_inactive]  DEFAULT ('N') FOR [inactive]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_created_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_created_by]  DEFAULT ([dbo].[fs_user]()) FOR [created_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_create_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_create_dt]  DEFAULT (getdate()) FOR [create_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_create_loc]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_create_loc]  DEFAULT (host_name()) FOR [create_loc]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_last_updated_by]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_last_updated_by]  DEFAULT ([dbo].[fs_user]()) FOR [last_updated_by]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_last_update_dt]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_last_update_dt]  DEFAULT (getdate()) FOR [last_update_dt]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LTR_WEB_DONATION_CONFIG_donate_by_default]') AND type = 'D') BEGIN
    ALTER TABLE [dbo].[LTR_WEB_DONATION_CONFIG] ADD  CONSTRAINT [DF_LTR_WEB_DONATION_CONFIG_variant_id]  DEFAULT (0) FOR [donate_by_default]
END
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[LTR_WEB_DONATION_CONFIG] TO impusers
GO

EXECUTE [dbo].[UP_POPULATE_REFERENCE_METADATA] @table_name = 'LTR_WEB_DONATION_CONFIG'
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LTR_WEB_DONATION_CONFIG_TU]'))
    DROP TRIGGER [dbo].[LTR_WEB_DONATION_CONFIG_TU]
GO

CREATE TRIGGER [dbo].[LTR_WEB_DONATION_CONFIG_TU] ON [dbo].[LTR_WEB_DONATION_CONFIG] FOR UPDATE
AS BEGIN

    Set NoCount On  

    --Update the last_updated_by, last_update_dt columns
    UPDATE 	[dbo].[LTR_WEB_DONATION_CONFIG]
    SET 	last_updated_by = dbo.FS_USER(), last_update_dt = getdate()
    FROM	[dbo].[LTR_WEB_DONATION_CONFIG] a, inserted b
    WHERE	a.id = b.id
 
END
GO

INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG] ([variant_id], [rank], [constituent_no], [message_title], [description], [method_no], [fund_id], [prod_season_no]) VALUES (1, 1, 0, 'Support Us 1', 'Support Us', 'percentage', 1, 0)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG] ([variant_id], [rank], [constituent_no], [message_title], [description], [method_no], [fund_id], [prod_season_no]) VALUES (1, 2, 0, 'Support Us 2', '', 'flat', 1, 0)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG] ([variant_id], [rank], [constituent_no], [message_title], [description], [method_no], [fund_id], [prod_season_no]) VALUES (1, 3, 0, 'Support Us 3', '', 'round', 1, 0)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG] ([variant_id], [rank], [constituent_no], [message_title], [description], [method_no], [fund_id], [prod_season_no]) VALUES (1, 0, 386002, 'Dear Customer', '', 'ask_amount', 1, 0)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG] ([variant_id], [rank], [constituent_no], [message_title], [description], [method_no], [fund_id], [prod_season_no]) VALUES (1, 2, 0, '', '', 'percentage', 1, 0)
INSERT INTO [dbo].[LTR_WEB_DONATION_CONFIG] ([variant_id], [rank], [constituent_no], [message_title], [description], [method_no], [fund_id], [prod_season_no]) VALUES (2, 2, 0, '', '', 'round', 1,123)
GO

SELECT * FROM [dbo].[LTR_WEB_DONATION_CONFIG]